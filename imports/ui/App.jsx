import React, {Component} from 'react';
import Welcome from "./Welcome"

export default class App extends Component {
    render() {
        return (
            <div id="app">
                <Welcome/>
            </div>
        )
    }
}