import React, {Component} from 'react';

export default class Welcome extends Component {
    render() {
        return (
            <div id="welcome">
                <div className="background">
                    <div className="cover">
                        <div id="welcome-particles"></div>
                        <div className="content">
                            <div className="logo"></div>
                            <div className="introduction">
                                <div className="name">Dmitry Romanov</div>
                                <div className="caption">Fullstack web developer</div>
                            </div>
                            <div className="navigation">
                                <div className="navigation-entry active">About</div>
                                <div className="navigation-entry">Blog</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    componentDidMount() {
        particlesJS.load("welcome-particles", "particles/welcome.json")
    }
}